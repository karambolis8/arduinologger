//
//#define BUTTON_INPUT 9
//#define BUTTON_DELAY 200

#define BUTTON_AUTO 5000

#define READOUTS_REFRESH 500

#define ANALOG_READ_DELAY 50

//RX-TX
#define GPS_BAUD 9600
//#define GPS_BAUD 115200
#define GPS_MIN_SAT 12
#define GPS_ZEROING_TIME 40
#define GPS_REFRESH 1000

//I2C
#define OLED
#define OLED_REFRESH 1000
#define OLED_SENSOR_CALIBRATION_DELAY 500

//SPI
//#define SD_CARD
//#define SD_LOG_TIME 1000

//I2C
//#define GYRO

//Analog A0
#define TEMP
//#define LM35
#define TMP36

//Analog A7
//#define AIRSPEED

//I2C
//#define BME280

//Analog A1
//#define CURRENT_SENSOR
//#define ACS758_50B
//#define ACS712_20B
